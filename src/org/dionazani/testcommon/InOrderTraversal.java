package org.dionazani.testcommon;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode() {
	}

	TreeNode(int val) {
		this.val = val;
	}
	
	TreeNode(int val, TreeNode left, TreeNode right) {
		         this.val = val;
		          this.left = left;
		          this.right = right;
		      }

}

public class InOrderTraversal {

	public static List<Integer> inOrderTraversal(TreeNode root) {
        List<Integer> l = new ArrayList<>();
        if (root == null) return l;
        funcPostOrder(root, l);
        
        return l;
    }

    private static void funcPostOrder(TreeNode c, List<Integer> l) {
        if ((c.left != null) && (c.left.val != 0)) {
            funcPostOrder(c.left, l);
        }
        if (c.right != null) {
            funcPostOrder(c.right, l);
        }
        
        l.add(c.val);
    }
	
	public static TreeNode fromArray(Integer[] tree) {
        if (tree.length == 0) return null;
        TreeNode root = new TreeNode(tree[0]);
        Queue<TreeNode> q = new LinkedList<>();
        q.add(root);
        
        for (int i = 1; i < tree.length; i++) {
        	
        	try {
        		TreeNode node = q.peek();
                if (node.left == null) {
                    node.left = new TreeNode(tree[i]);
                    if (tree[i] != null) q.add(node.left);
                } else if (node.right == null) {
                    node.right = new TreeNode(tree[i]);
                    if (tree[i] != null) q.add(node.right);
                    q.remove();
                }
        	}
        	catch(NullPointerException npe) {
        		continue;
        	}
        }
        
        return root;
    }

	public static void main(String[] args) {
		Integer[] tree = new Integer[]{1, null, 2, 3};
		
        TreeNode treeNode = fromArray(tree);
        List<Integer> list = inOrderTraversal(treeNode);
        
        list.forEach(System.out::println); 

	}
}
