package org.dionazani.testcommon;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TwoSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
			Apabila ingin INPUT dari TERMINAL, remove comment dibawah ini.
		*/
		
		/*
		Scanner sc = new Scanner(System.in);
		
	    int size;
	    System.out.println("Enter the number of size of array: ");
	    size = sc.nextInt();
	   
	    String str = "";
	    System.out.println("Enter the array element (separate with comma): ");
	    str = sc.next();
	    
	    // For reading the element
	    int[] nums = new int[size];
	    int i=0;
	    String[] items = str.split(",");
	 
	    for(String item:items) {
	    	nums[i] = Integer.parseInt(item);
 	        i++;
	    }
	    
	    int target;
	    System.out.println("Enter the number target: ");
	    target = sc.nextInt();
	    
	    System.out.println("");
	    System.out.println("RESULTS : ");
		twoSum(nums, target);
		*/
		
		// Remove code dibawah ini apabila ingin dapat INPUT dari TERMINAL.
		
        int[] nums = {2, 7, 2, 11, 15};
        int target = 9;
        
        System.out.println("RESULTS : ");
		twoSum(nums, target);
		
	}
	
	public static void twoSum(int[] nums, int target) {
		
	    Map<Integer, Integer> map = new HashMap<Integer, Integer>();
	    
	    for (int i = 0; i < nums.length; i++) {
	        map.put(nums[i], i);
	    }

	    int[] temps = new int[2];
	    for (int i = 0; i < nums.length; i++) {
	        int complement = target - nums[i];
	        if (map.containsKey(complement) && map.get(complement) != i) {
	        	
	        	int[] results = { i, map.get(complement) };
        		Arrays.sort(results);
        		if (!Arrays.equals(temps, results)) {
            		temps = results;
            		System.out.println(Arrays.toString(temps));
        		}
	        }
	    }
	}
	
}
